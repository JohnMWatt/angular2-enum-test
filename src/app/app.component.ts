import { Component } from '@angular/core';
import {SectionEnumAware} from "./section-aware.decorator";
import {Section} from "./section";

@Component({
  selector: 'my-app',
  template: `<h1>Hello {{name}}</h1>
              {{Section[section]}}
  <select>
    <option *ngFor="let d of enumData" [value]="d" [selected]="d === Section.END">{{Section[d]}}</option>
  </select>`
              ,
})

@SectionEnumAware
export class AppComponent  {
  name = 'First';
  section: Section = Section.START;
  data: String[] = ['First', 'Second', 'Third'];
  enumData: Section[] = [Section.START, Section.END];
}
