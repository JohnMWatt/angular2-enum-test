import {Section} from './section';

export function SectionEnumAware(constructor: Function) {
  constructor.prototype.Section = Section;
}
